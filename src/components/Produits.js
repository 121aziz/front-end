import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import * as React from "react";
import {
  Create, Datagrid,
  DeleteButton, Edit, EditButton,
  List, NumberField, NumberInput,
  SimpleForm,
  TextField, TextInput,
  useAuthenticated
} from 'react-admin';

export const ProduitsIcon = ShoppingCartIcon;

export const ProduitsList = (props) => {
  useAuthenticated();
  return (
    <List {...props}>
      <Datagrid>
        <TextField source="nom" />
        <NumberField source="prix" />
        <EditButton basePath="/produits" />
        <DeleteButton basePath="/produits" />
      </Datagrid>
    </List>
  );
}


export const ProduitsEdit = (props) => {
  useAuthenticated();
  return (
    <Edit title="Modifier Produit" {...props}>
      <SimpleForm>
        <TextInput disabled source="id" />
        <TextInput source="nom" />
        <NumberInput source="prix" />
      </SimpleForm>
    </Edit>
  );
}

export const ProduitsCreate = (props) => {
  useAuthenticated();
  return (
    <Create title="Create bureau" {...props}>
      <SimpleForm>
        <TextInput source="nom" />
        <NumberInput source="prix" />
      </SimpleForm>
    </Create>
  );
}
