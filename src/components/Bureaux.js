import AccountBoxIcon from '@material-ui/icons/AccountBox';
import * as React from "react";
import {
  BooleanField,

  BooleanInput, Create, Datagrid,
  DeleteButton, Edit, EditButton,
  List,
  SimpleForm,
  TextField, TextInput,
  useAuthenticated
} from 'react-admin';

export const BureauxIcon = AccountBoxIcon;

export const BureauxList = (props) => {
  useAuthenticated();
  return (
    <List {...props}>
      <Datagrid>
        <TextField source="idbct" label="ID BCT" />
        <TextField source="adresse" label="Adresse" />
        <TextField source="nombureau" label="Nom Bureau" />
        <TextField source="nom" label="Nom" />
        <TextField source="prenom" label="Prenom" />
        <TextField source="email" label="Email" />
        <TextField source="cin" label="CIN" />
        <BooleanField source="actif" />
        <BooleanField source="visible" />
        <EditButton basePath="/bureaux" />
        <DeleteButton basePath="/bureaux" />
      </Datagrid>
    </List>
  );
}


export const BureauxEdit = (props) => {
  useAuthenticated();
  return (
    <Edit title="Edit bureau" {...props}>
      <SimpleForm>
        <TextInput source="id" disabled fullWidth />
        <TextInput source="idbct" label="ID BCT" fullWidth />
        <TextInput source="adresse" label="Adresse" fullWidth />
        <TextInput source="nombureau" label="Nom Bureau" fullWidth />
        <TextInput source="nom" label="Nom" fullWidth />
        <TextInput source="prenom" label="Prenom" fullWidth />
        <TextInput source="email" label="Email" fullWidth type="email" />
        <TextInput source="mdp" label="MDP" fullWidth type="password" />
        <TextInput source="cin" label="CIN" fullWidth />
        <BooleanInput source="actif" />
        <BooleanInput source="visible" />
      </SimpleForm>
    </Edit>
  );
}

export const BureauxCreate = (props) => {
  useAuthenticated();
  return (
    <Create title="Create bureau" {...props}>
      <SimpleForm>
        <TextInput source="idbct" label="ID BCT" fullWidth />
        <TextInput source="adresse" label="Adresse" fullWidth />
        <TextInput source="nombureau" label="Nom Bureau" fullWidth />
        <TextInput source="nom" label="Nom" fullWidth />
        <TextInput source="prenom" label="Prenom" fullWidth />
        <TextInput source="email" label="Email" fullWidth type="email" />
        <TextInput source="mdp" label="MDP" fullWidth type="password" />
        <TextInput source="cin" label="CIN" fullWidth />
        <BooleanInput source="actif" />
        <BooleanInput source="visible" />
      </SimpleForm>
    </Create>
  );
}
