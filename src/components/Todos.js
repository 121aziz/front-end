import BookIcon from '@material-ui/icons/Book';
import * as React from "react";
import { BooleanField, BooleanInput, Create, Datagrid, DeleteButton, Edit, EditButton, List, SimpleForm, TextField, TextInput, useAuthenticated } from 'react-admin';
export const TodosIcon = BookIcon;

export const TodosList = (props) => {
  useAuthenticated();
  return (
    <List {...props}>
      <Datagrid>
        <TextField source="id" />
        <TextField source="name" />
        <BooleanField source="completed" />
        <EditButton basePath="/todos" />
        <DeleteButton basePath="/todos" />
      </Datagrid>
    </List>
  );
}


export const TodosEdit = (props) => {
  useAuthenticated();
  return (
    <Edit title="Edit client" {...props}>
      <SimpleForm>
        <TextInput disabled source="id" />
        <TextInput source="name" />
        <BooleanInput label="completed" source="completed" />
      </SimpleForm>
    </Edit>
  );
}

export const TodosCreate = (props) => {
  useAuthenticated();
  return (
    <Create title="Create a client" {...props}>
      <SimpleForm>
        <TextInput source="name" />
        <BooleanInput label="completed" source="completed" />
      </SimpleForm>
    </Create>
  );
}
