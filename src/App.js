import { Admin, Resource } from 'react-admin';
import authProvider from './authProvider';
import { BureauxCreate, BureauxEdit, BureauxIcon, BureauxList } from './components/Bureaux';
import MyLoginPage from './components/MyLoginPage';
import { ProduitsCreate, ProduitsEdit, ProduitsIcon, ProduitsList } from './components/Produits';
import myDataProvider from './dataProvider';




function App() {
  // https://marmelab.com/react-admin/Fields.html#reference-fields

  return (
    <div>
      <Admin
        dataProvider={myDataProvider('http://localhost:5000')}
        authProvider={authProvider}
        loginPage={MyLoginPage}
      //logoutButton={MyLogoutButton}
      >
        <Resource
          name='bureaux'
          list={BureauxList}
          edit={BureauxEdit}
          create={BureauxCreate}
          icon={BureauxIcon}
          options={{ label: 'Bureaux' }}
        />
        <Resource
          name='produits'
          list={ProduitsList}
          edit={ProduitsEdit}
          create={ProduitsCreate}
          icon={ProduitsIcon}
          options={{ label: 'Produits' }}
        />
      </Admin>
      {console.log(localStorage.getItem('decodedToken'))}
    </div>

  );
}

export default App;
